d3.select('#prueba').style('color', 'red');

const dataset = [1, 2, 3, 4];
const texto = d3.select('body')
  .selectAll('.texto')
  .data(dataset)
  .enter()
  .append('p');

  texto
  .attr('class', 'texto')
  // .text(d => `Hola ${d}`);
  .text(function(d) {
    return 'Hola ' + d;
  })