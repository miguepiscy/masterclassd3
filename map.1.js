const height = 1000;
const width = 1000;
const color = d3.scaleOrdinal(d3.schemeCategory10)

const svg = d3.select('#prueba')
  .append('svg')
  .attr('width', width)
  .attr('height', height);

d3.json('/provinces.json')
  .then((provinces) => {
    const projection = d3.geoMercator()
      .scale(height * 2)
      .center([-3.703526, 40.416992])
      .translate([width/2, height/2]);
    const path = d3.geoPath().projection(projection);

    const featuresProvincias = topojson.feature(provinces, provinces.objects.provincias).features;
    const provincias = svg.selectAll('.provincias')
      .data(featuresProvincias)
      .enter()
      .append('path');

    provincias.attr('d', path)
      .attr('fill', d => color(d.properties.NATCODE));

    const featuresAutonomicas = topojson.feature(provinces, provinces.objects.autonomicas).features;
    const autonomicas = svg.selectAll('.autonomicas')
      .data(featuresAutonomicas)
      .enter()
      .append('path');

    autonomicas.attr('d', path)
      .attr('fill', d => {
        d.opacity = 1;
        return color(d.properties.CODNUT2)
      })
      .on('click', function (d) {
        console.log('hola', d);
        d.opacity = d.opacity === 1 ? 0 : 1;
        d3.select(this).attr('opacity', d.opacity );
      });

    //  diferencia entre function y arrow fuction
    //   function parent() {
    //     this.a = 'hola';
    //     children(this);
    //     function children(self) {
    //       console.log(self.a);
    //     }
    //   }

    // function parent() {
    //   this.a = 'hola';
    //   children();
    //   const children = () => {
    //     console.log(this.a);
    //   }
    // }

    
  });