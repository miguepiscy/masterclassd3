const api = 'https://api.coindesk.com/v1/bpi/historical/close.json';

function parserData(data) {
  const arr = [];
  for (let i in data.bpi) {
    arr.push({
      date: new Date(i),
      value: data.bpi[i],
    })
  }
  return arr;
}

d3.json(api)
  .then((data) => {
    const parser = parserData(data);
    drawChart(parser);
  });

function drawChart(data) {
  const svgWidth = 600;
  const svgHeight = 400;
  const marginBottom = 20;
  const marginLeft = 40;

  const svg = d3.select('#prueba')
    .append('svg')
    .attr('width', svgWidth)
    .attr('height', svgHeight);
  
  const x = d3.scaleTime()
    .domain(d3.extent(data, d => d.date))
    .range([marginLeft, svgWidth]);

  const y = d3.scaleLinear()
    .domain(d3.extent(data, d => d.value))
    .range([svgHeight - marginBottom, 0]);

  const line = d3.line()
    .x(d => x(d.date))
    .y(d => y(d.value));

  const g = svg.append('g')
  
  const axisX = d3.axisBottom(x).ticks(5);

  g.append('g')
    .attr('transform', `translate(0, ${svgHeight - marginBottom})`)
    .call(axisX);
  
  const axisL = d3.axisLeft(y).ticks(4);

  g.append('g')
    .call(axisL)
    .attr('transform', `translate(${marginLeft})`);

  g.append('path')
    .attr('fill', 'none')
    .attr('stroke', 'steelblue')
    .attr('stroke-width', 1.5)
    .attr('d', line(data));
}