const data = [
  [5, 20],
  [480, 90],
  [250, 50],
  [100, 33],
  [330, 95],
  [410, 12],
  [475, 44],
];

const width = 500;
const height = 500;
ratio = 5;
const xmax = d3.max(data, d => d[0]);
const xmin = d3.min(data , d => d[0]);

const scaleX = d3.scaleLinear()
  .domain([xmin, xmax])
  .range([ratio, width - ratio ]);

const ymax = d3.max(data, d => d[1]);
const ymin = d3.min(data, d => d[1]);

const scaleY = d3.scaleLinear()
  .domain([ymin, ymax])
  .range([ratio, height - ratio])

const svg = d3.select('#prueba')
  .append('svg')
  .attr('width', width)
  .attr('height', height);

const group = svg.selectAll('g')
  .data(data)
  .enter()
  .append('g');

const circle = group.append('circle');

circle
  .attr('cx', d => scaleX(d[0]))
  .attr('cy', d => scaleY(d[1]))
  .attr('r', (d) => {
    const radio = parseInt(d[1] / 10);
    d.push(radio);
    return radio;
  });

circle.attr('class', (d) => {
    console.log(d);
    const radio = d[2];
    if (radio > 5) {
      return 'red';
    }
  });

const text = group.append('text');
function texto(d) {
  return `${d[0]}-${d[1]}`;
}
text.text(texto)
  .attr('x', d => {
    const scale = scaleX(d[0])
    const txtLength = texto(d).length + 50;
    if (scale > width - txtLength) {
      return scale - txtLength;
    }
    return scale;
  })
  .attr('y', d => scaleY(d[1]))

  const xAxis = d3.axisBottom(scaleX);
  svg.append('g')
    .attr('class', 'axisX')
    .attr('transform', `translate(0, ${height - 20})`)
    .call(xAxis)