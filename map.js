//  install gdal (brew install gdal) && topojson (npm install -g topojson)
//  ogr2ogr -f GeoJson subunits.json -where "ADM0_A3 IN ('ESP')" ne_10m_admin_0_map_subunits.shp
//  ogr2ogr -f GeoJson places.json -where "ADM0_A3 IN ('ESP')" xxx_places.shp
// topojson subunits.json places.json - o spain.json
const height = 1000;
const width = 1000;
const svg = d3.select('#prueba')
  .append('svg')
  .attr('width', width)
  .attr('height', height);

d3.json('/spain.json')
  .then((spain) => {
    const projection = d3.geoMercator()
      .scale(height)
      .center([-3.703526, 40.416992])
      .translate([width/2, height/2]);
    const path = d3.geoPath().projection(projection);
    const featuresSubunits = topojson.feature(spain, spain.objects.subunits).features;
    const subunits = svg.selectAll('.subunits')
      .data(featuresSubunits)
      .enter()
      .append('path');
    
    subunits
      .attr('d', path)
      .attr('class', (d) => {
        console.log(d.properties.POSTAL);
        return `postal_${d.properties.POSTAL}`;
      });
    
    const featuresPlaces = topojson.feature(spain, spain.objects.places).features;
    const group = svg
      .selectAll('.places')
      .data(featuresPlaces)
      .enter()
      .append('g')
      .filter((d) => {
        return d.properties.NAME === 'Madrid';
      });
    
    group.attr('transform', (d) => {
        const translate = projection(d.geometry.coordinates);
        return `translate(${translate})`;
      });
    
    const places = group.append('circle');
    places
      .attr('r', 2);
    
    const textPlace = group.append('text');
    textPlace
      .text((d) => {
        return d.properties.NAME;
      })
  });