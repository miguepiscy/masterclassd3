const svg = d3.select('#prueba')
  .append('svg');

svg
  .attr('width', 500)
  .attr('height', 100);

const data = [5, 10, 15, 20, 25, 20, 10];

const circle = svg.selectAll('circle')
  .data(data)
  .enter()
  .append('circle');
const max = d3.max(data);

circle
  .attr('cx', (d, i) => {
    return (i * max * 2) + max;
  })
  .attr('cy', max)
  .attr('r', d => d);