const data = [5, 10, 15, 20, 25];
const height = 200;
const svg = d3.select('#prueba')
  .append('svg')
  .attr('with', 500)
  .attr('height', height);

const rect = svg.selectAll('rect')
  .data(data)
  .enter()
  .append('rect');

const rectw = 20;
rect
  .attr('x', posX)
  .attr('y', posY)
  .attr('width', rectw)
  // .attr('height', scale);
  .attr('height', scale)
  .attr('class', (d) => {
    if (d > 10) {
      return 'red';
    }
  });

const text = svg.selectAll('text')
  .data(data)
  .enter()
  .append('text');

text.text(d => d)
  .attr('x', posX)
  .attr('y', posY);

function posX(d, i) {
  return i * (rectw + 1);
}

function posY(d) {
  return height - scale(d);
}
function scale(d) {
  const scaleNum = height / d3.max(data);
  return scaleNum * d - 20;
}