const data = [
  {'platform': 'Android', 'percentage': 80},
  {'platform': 'Ios', 'percentage': 19 },
  {'platform': 'Windows', 'percentage': 1 },
];

const width = 500;
const height = 500;
const ratio = Math.min(width, height) / 2;
const svg = d3.select('#prueba')
  .append('svg')
  .attr('width', width)
  .attr('height', height);

const pie = d3.pie().value((d) => d.percentage);

const arc = d3.arc()
  .outerRadius(ratio)
  .innerRadius(0);

const g = svg.append('g');
g.attr('transform', `translate(${ratio}, ${ratio})`)

const group = g.selectAll('arc')
  .data(pie(data))
  .enter()
  .append('g');

const color = d3.scaleOrdinal(d3.schemeCategory10)

group.append('path')
  .attr('d', arc)
  .attr('fill', d => color(d.data.percentage))

group.append('text')
  .attr('transform', (d) => {
    const center = arc.centroid(d);
    return `translate(${center[0]}, ${center[1]})`;
  })
  .text(d => `${d.data.platform}, ${d.data.percentage}`)
