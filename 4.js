const data = [
  [5, 20],
  [480, 90],
  [250, 50],
  [100, 33],
  [330, 95],
  [410, 12],
  [475, 44],
];

const width = 500;
const height = 500;
ratio = 5;
const xmax = d3.max(data, d => d[0]);
const xmin = d3.min(data , d => d[0]);

const scaleX = d3.scaleLinear()
  .domain([xmin, xmax])
  .range([ratio, width - ratio ]);

const ymax = d3.max(data, d => d[1]);
const ymin = d3.min(data, d => d[1]);

const scaleY = d3.scaleLinear()
  .domain([ymin, ymax])
  .range([ratio, height - ratio])

const svg = d3.select('#prueba')
  .append('svg')
  .attr('width', width)
  .attr('height', height);

const circle = svg.selectAll('circle')
  .data(data)
  .enter()
  .append('circle');

circle
  .attr('cx', d => scaleX(d[0]))
  .attr('cy', d => scaleY(d[1]))
  .attr('r', ratio);
